# -*- coding: utf-8 -*-
# @Time    : 2021-01-10 12:16 p.m.
# @Author  : Galefang A. Mapunda and Spiro Sarris
# @Email   : galefang@narit.or.th and spiro@narit.or.th
# @Project : Python
# @File    : correlation_concept.py
# @Software: PyCharm
import numpy as np
import random
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use("QT5agg")

plt.rcParams["savefig.directory"] = "/home/galefang/Documents/Gale/DirectionFinding/Plots/"
plt.rcParams.update({'figure.max_open_warning': 30})
plt.rcParams.update({'font.size': 16})
plt.rc('font', **{'family': 'serif', 'serif': ['Palatino']})
plt.rc('text', usetex=True)

# plt.style.use('dark_background')

save_to = "/home/galefang/Documents/Gale/DirectionFinding/Plots/"


def measured_angles(numberofantennas):

    if numberofantennas == 3:

        random.seed(30)

        measured_phases = np.array(random.sample(range(-180, 180), numberofantennas))  # ambigious phase delays

        # print("Measured Phase angles at one instance are \n", measured_phases)

    elif numberofantennas == 5:

        random.seed(30)

        measured_phases = np.array(random.sample(range(-180, 180), numberofantennas))  # ambigious phase delays

        # print("Measured Phase angles at one instance are \n", measured_phases)

    elif numberofantennas == 9:

        random.seed(30)

        measured_phases = np.array(random.sample(range(-180, 180), numberofantennas))  # ambigious phase delays

        # print("Measured Phase angles at one instance are \n", measured_phases)

    # measuredphases = measured_phases

    return measured_phases


def pre_computed_angles(numberofantennas):

    if numberofantennas == 3:

        pre_computed = random.sample(range(-180, 180, 1), numberofantennas - 2)

        pre_computed.append(0)

        pre_computed.append(-1 * pre_computed[0])

        # print("Pre-computed angles", pre_computed)

    elif numberofantennas == 5:

        pre_computed = random.sample(range(-180, 180, 2), numberofantennas - 3)

        pre_computed.append(0)

        [pre_computed.append(-1 * pre_computed[i]) for i in range(0, len(pre_computed)-1)]

        pre_computed[3], pre_computed[4] = pre_computed[4], pre_computed[3]

        # print("Pre-computed angles", pre_computed)

    elif numberofantennas == 9:

        pre_computed = random.sample(range(-180, 180, 3), numberofantennas - 5)

        pre_computed.append(0)

        [pre_computed.append(-1 * pre_computed[i]) for i in range(0, len(pre_computed)-1)]

        pre_computed[5], pre_computed[6], pre_computed[7], pre_computed[8] = pre_computed[8], pre_computed[7], pre_computed[6], pre_computed[5]

        # print("Pre-computed angles", pre_computed)
    else:
        pass

    # precomputedphases = pre_computed

    return pre_computed


def correlation(ambigious_angles, unambigious_angles, correlative_resolution):

    cost = []
    for res in range(len(correlative_resolution)):
        jmle = np.sum(list(np.cos(ambigious_angles - unambigious_angles[res])))
        cost.append(jmle)

    cost_function = cost

    print("Joint maximum likelihood \n", cost_function)
    print("____________________________________")

    return cost_function


number_of_antennas = 5
print("Total number of antennae is \n", number_of_antennas)
print("____________________________________")

search_range = [-30, 30]

resolution_angle = 5

search_resolution = np.array(np.arange(search_range[0], search_range[1]+1, resolution_angle))

print("Correlation resolution \n", len(search_resolution))
print("____________________________________")

ambigious_phases = measured_angles(number_of_antennas)
print("Ambigious (measured) phases \n", ambigious_phases)
print("____________________________________")

unambigious_phases = np.array([pre_computed_angles(number_of_antennas) for i in range(len(search_resolution))])
print("Unambigious (pre-computed) phases \n", unambigious_phases)
print("____________________________________")

cost_function = correlation(ambigious_phases, unambigious_phases, search_resolution)
print("Maximum value \n", max(cost_function))
print("____________________________________")

# -------------------------Plot the correlation function-------------------------------- #
fig = plt.figure(figsize=(10, 4), clear=True)
figax = fig.add_subplot(111)
figax.set_xlabel("Corresponding AOA")
figax.set_ylabel("Cost Function $J\\left(\\theta\\right)$")
plt.axhline(y=0, color='k')
plt.plot(search_resolution, cost_function, 'b-+', drawstyle='steps-pre')
# plt.plot(search_resolution, cost_function, 'r-*')
plt.minorticks_on()
plt.tight_layout(pad=0.5)
# plt.title("Output pattern")
plt.xlim([np.min(search_resolution), np.max(search_resolution)])
plt.ylim([None, None])
plt.grid(True)
plt.savefig(save_to + "%d_elelment_" % number_of_antennas + "%.1fdeg_costfunction.png" % resolution_angle, bbox_inches='tight', transparent=True, dpi=200)
plt.show()
